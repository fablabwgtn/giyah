// Giyah code
// Written by Harry Iliffe

#include <DHTesp.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Encoder.h>

#define HEATER_OFF 0
#define HEATER_HALF 1
#define HEATER_ON 2

#define MAX_TEMP 50
#define MIN_TEMP 5

#define PIN_TEMP 16
#define PIN_HEATER_1 18
#define PIN_HEATER_2 19

#define SDA 32
#define SCL 33
#define ENC_1 15
#define ENC_2 4
#define ENC_BUTTON 13

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

DHTesp dht;

Encoder encoder(ENC_1, ENC_2);

double encoderValue;
int heaterState = 0;



bool settingTemp = false;
int lastButtonState;    // the previous state of button
int currentButtonState; // the current state of button

float tempValue = 0;

void updateTemp(void * parameters );
void updateDisplay(void * parameters);

void setup() {
  Serial.begin(115200);
  Serial.println("GIYAH V2.0.beta2");
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PIN_HEATER_1, OUTPUT);
  pinMode(PIN_HEATER_2, OUTPUT);
  
  pinMode(ENC_BUTTON, INPUT);

  pinMode(23, OUTPUT); //THIS IS FOR THE TEMP POWER SUPPLY
  digitalWrite(23, HIGH); //LEAVE PULLED HIGH!
    digitalWrite(PIN_HEATER_1, LOW);
    digitalWrite(PIN_HEATER_2, LOW);

  dht.setup(PIN_TEMP, DHTesp::DHT11);

  Wire.begin(SDA, SCL);
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C, true, false)) { // Address 0x3D for 128x64
    Serial.println("Couldn't connect to display");
    for (;;); // Don't proceed, loop forever
  }

  display.clearDisplay();

  xTaskCreate(
    updateTemp,    // Function that should be called
    "Update Temp/Heater",   // Name of the task (for debugging)
    1000,            // Stack size (bytes)
    NULL,            // Parameter to pass
    1,               // Task priority
    NULL             // Task handle
  );

  xTaskCreate(
    updateDisplay,    // Function that should be called
    "Update Display/Encoder",   // Name of the task (for debugging)
    1000,            // Stack size (bytes)
    NULL,            // Parameter to pass
    3,               // Task priority
    NULL             // Task handle
  );
  xTaskCreate(
    updateHeater,    // Function that should be called
    "Update Heater",   // Name of the task (for debugging)
    1000,            // Stack size (bytes)
    NULL,            // Parameter to pass
    2,               // Task priority
    NULL             // Task handle
  );
  currentButtonState = digitalRead(ENC_BUTTON);
}

void updateTemp(void * parameters ) {
  Serial.println("Starting Update Temp Task");
  for (;;) {

    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)

    TempAndHumidity tempNew = dht.getTempAndHumidity();
    if (dht.getStatus() == 0) {
//      Serial.print("Humidity: ");
//      Serial.print(tempNew.humidity);
//      Serial.print(" %\t");
//      Serial.print("Temperature: ");
//      Serial.print(tempNew.temperature);
//      Serial.println(" *C");
      tempValue = tempNew.temperature;
    } else {
      Serial.println("Failed to get temperature and humidity value.");
      Serial.println("DHT11 error status: " + String(dht.getStatusString()));
    }

    vTaskDelay(1500 / portTICK_PERIOD_MS);
  }
}


void updateDisplay(void * parameters) {
  Serial.println("Starting Update Display Task");
  static float delta = 0;

  for (;;) {
    if (delta != tempValue + encoderValue + settingTemp || settingTemp) {
      delta = tempValue + encoderValue + settingTemp;
      display.clearDisplay();
      display.setTextSize(2);             // Normal 1:1 pixel scale
      display.setTextColor(SSD1306_WHITE);        // Draw white text
      display.setCursor(0, 0);            // Start at top-left corner

      if (settingTemp) {
        float encoderValueTemp = encoder.read();
        encoderValueTemp = encoderValueTemp / 4;
        encoderValue = constrain(encoderValueTemp, MIN_TEMP, MAX_TEMP);

        display.print(F("Temp: "));
        display.println(tempValue, 0);
        display.setTextSize(2);
        display.println(F("Set Temp:"));
        display.setTextSize(4);
        display.println(encoderValue, 0);
      } else {
        display.println(F("Temp:"));
        display.setTextSize(6);             // Draw 2X-scale text
        display.println(tempValue, 0);
      }
      display.display();
    }

    vTaskDelay(100 / portTICK_PERIOD_MS);
  }
}

void updateHeater(void * parameters) {
  Serial.println("Starting Update Heater Task");
  for (;;) {
    if (tempValue >= encoderValue && heaterState != HEATER_OFF ) {
      Serial.println("Heating OFF");
      heaterState = HEATER_OFF;
      digitalWrite(PIN_HEATER_1, LOW);
      digitalWrite(PIN_HEATER_2, LOW);
      digitalWrite(LED_BUILTIN, LOW);
    } else if (tempValue < encoderValue && encoderValue - tempValue <= 2 && heaterState != HEATER_HALF) {
      Serial.println("Heating HALF ON");
      heaterState = HEATER_HALF;
      digitalWrite(PIN_HEATER_1, HIGH);
      digitalWrite(PIN_HEATER_2, LOW);
      digitalWrite(LED_BUILTIN, HIGH);
    } else if (tempValue < encoderValue && encoderValue - tempValue > 2 && heaterState != HEATER_ON)  {
      Serial.println("Heating ON");
      heaterState = HEATER_ON;
      digitalWrite(PIN_HEATER_1, HIGH);
      digitalWrite(PIN_HEATER_2, HIGH);
      digitalWrite(LED_BUILTIN, HIGH);
    }
    vTaskDelay(100 / portTICK_PERIOD_MS);
  }
}

void loop() {
  lastButtonState    = currentButtonState;      // save the last state
  currentButtonState = digitalRead(ENC_BUTTON); // read new state

  if (lastButtonState == HIGH && currentButtonState == LOW) {
    //Serial.println("The button is pressed");
    settingTemp = !settingTemp;
  }

  yield();
}
