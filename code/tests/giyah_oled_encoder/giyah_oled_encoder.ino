// include the library code:
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Encoder.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

Encoder myEnc(4, 15);

void setup() {
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  Wire.begin(32, 33);
  Serial.begin(115200);
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C, true, true)) { // Address 0x3D for 128x64
    Serial.println("Couldn't connect to display");
    for(;;); // Don't proceed, loop forever
  }
  // Print a message to the LCD.
  display.clearDisplay();
  Serial.println("Setup complete");

}

double oldPosition  = -999;

void loop() {
  double newPosition = myEnc.read()/2;
  if (newPosition != oldPosition) {
    oldPosition = newPosition;
  }
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  display.clearDisplay();
  display.setTextSize(2);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);             // Start at top-left corner
  display.println(F("Giyah!"));
  display.setTextSize(6);             // Draw 2X-scale text
  display.setTextColor(SSD1306_WHITE);
  display.println(round(newPosition));
  display.display();
  delay(20);
}
